//
//  DetailView.swift
//  News Reader
//
//  Created by Arjay on 10/1/20.
//  Copyright © 2020 Arjay. All rights reserved.
//

import UIKit

class DetailView: UIViewController {
    
    @IBOutlet var detailImage: UIImageView!
    @IBOutlet var detailTitle: UILabel!
    @IBOutlet var detailDesc: UILabel!
    
    var newsDetail: Article?

    override func viewDidLoad() {
        super.viewDidLoad()

        detailTitle.text = newsDetail?.title
        detailDesc.text = newsDetail?.description
        let imageUrl = newsDetail?.urlToImage
        if imageUrl == nil{
            detailImage.image = UIImage(named: "placeholder")
        }else{
            detailImage.sd_setImage(with: URL(string: imageUrl!))
        }
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
