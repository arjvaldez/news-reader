//
//  Article.swift
//  
//
//  Created by Arjay on 9/30/20.
//

import Foundation

struct Feed: Codable {
    var status: String = ""
    var totalResults: Int = 0
    var articles:[Article]?
}


struct Article: Codable {
    var author: String?
    var title: String?
    var description: String?
    var url: String?
    var urlToImage: String?
    var publishedAt: String?
    var content: String?
}
