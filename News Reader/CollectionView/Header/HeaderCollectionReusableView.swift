//
//  HeaderCollectionReusableView.swift
//  News Reader
//
//  Created by Arjay on 10/1/20.
//  Copyright © 2020 Arjay. All rights reserved.
//

import UIKit

class HeaderCollectionReusableView: UICollectionReusableView {
    
    let horizontalController = HorizontalCollection()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(horizontalController.view)
        horizontalController.view.translatesAutoresizingMaskIntoConstraints = false
        horizontalController.view.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        horizontalController.view.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        horizontalController.view.topAnchor.constraint(equalTo: topAnchor).isActive = true
        horizontalController.view.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    static let identifier = "HeaderCollectionReusableView"
    
    
}
