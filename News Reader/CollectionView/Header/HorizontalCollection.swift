//
//  HorizontalCollection.swift
//  News Reader
//
//  Created by Arjay on 10/1/20.
//  Copyright © 2020 Arjay. All rights reserved.
//

import UIKit

private let reuseIdentifier = "HorizontalCell"

class HorizontalCollection: HorizontalListController, UICollectionViewDelegateFlowLayout{

    var headlineFeed = [Article]()
    var didSelectItem: ((Article) ->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.backgroundColor = .white
        collectionView.register(UINib(nibName: "HorizontalCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 2.0
            layout.minimumInteritemSpacing = 2.0
            collectionView.collectionViewLayout = layout
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 150, height: 149)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return headlineFeed.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! HorizontalCell
        cell.headlineData = headlineFeed[indexPath.row]
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Select item \(indexPath.row)")
        didSelectItem?(headlineFeed[indexPath.row])
    }
    
}

class HorizontalCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    
    var headlineData: Article?{
        didSet{
            let imageUrl = headlineData?.urlToImage
            if imageUrl == nil{
                imageView.image = UIImage(named: "placeholder")
            }else{
                imageView.sd_setImage(with: URL(string: imageUrl!))
            }
            
            titleLabel.text = headlineData?.title
        }
    }
    
}

class HorizontalListController: UICollectionViewController {
        
        init() {
                super.init(collectionViewLayout: UICollectionViewFlowLayout())
        }
        
        required init?(coder aDecoder: NSCoder) {
                fatalError("init(coder:) has not been implemented")
        }
        
}
