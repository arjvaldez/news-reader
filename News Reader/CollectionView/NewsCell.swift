//
//  NewsCell.swift
//  News Reader
//
//  Created by Arjay on 9/30/20.
//  Copyright © 2020 Arjay. All rights reserved.
//

import UIKit

class NewsCell: UICollectionViewCell {
    
    @IBOutlet var newsImage: UIImageView!
    @IBOutlet var newsLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    static func nib() -> UINib{
        return UINib(nibName: "NewsCell", bundle: nil)
    }

}
