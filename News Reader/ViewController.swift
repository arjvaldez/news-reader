//
//  ViewController.swift
//  News Reader
//
//  Created by Arjay on 9/29/20.
//  Copyright © 2020 Arjay. All rights reserved.
//

import UIKit
import SDWebImage

class ViewController: UIViewController {
    
    @IBOutlet var collectionView: UICollectionView!
    
    let placeholderImage = UIImage(named: "placeholder")
    var headlineFeed = [Article]()
    var newsFeed = [Article](){
        didSet{
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(NewsCell.nib(), forCellWithReuseIdentifier: "NewsCell")
        collectionView.register(HeaderCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HeaderCollectionReusableView.identifier)
        collectionView.dataSource = self
        collectionView.delegate = self
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: self.view.frame.width, height: 70)
        layout.headerReferenceSize = CGSize(width: self.view.frame.width, height: 150)
        collectionView.collectionViewLayout = layout
        refreshFeed()
    }
    
    func refreshFeed(){
        headlineFeed.removeAll()
        newsFeed.removeAll()
        
        let request = ApiRequest()
        request.getNews { [weak self] result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let news):
                self?.newsFeed = news
            }
        }
        
        request.getHeadline { [weak self] result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let news):
                self?.headlineFeed = news
            }
        }
        
    }
    
}

extension ViewController: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        performSegue(withIdentifier: "segueToDetail", sender: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HeaderCollectionReusableView.identifier, for: indexPath) as! HeaderCollectionReusableView
        
            
        headerView.horizontalController.headlineFeed = self.headlineFeed
            headerView.horizontalController.didSelectItem = { [weak self] data in
                self?.performSegue(withIdentifier: "segueToDetail", sender: data)
            }
            headerView.horizontalController.collectionView.reloadData()
        print("\(headlineFeed.count)")
        
        return headerView

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToDetail" {
            if let destinationVC = segue.destination as? DetailView {
                if let indexPath = sender as? IndexPath {
                    destinationVC.newsDetail = newsFeed[indexPath.row]
                }
                if let data = sender as? Article {
                    destinationVC.newsDetail = data
                }
            }
        }
    }
    
}

extension ViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  "NewsCell", for: indexPath) as! NewsCell
        
        let news = self.newsFeed[indexPath.row]
        let imageURL: String? = news.urlToImage
        cell.newsLabel.text = news.title
        if imageURL == nil{
            cell.newsImage.image = placeholderImage
        }else{
            cell.newsImage.sd_setImage(with: URL(string: imageURL!))
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.newsFeed.count
    }
}


