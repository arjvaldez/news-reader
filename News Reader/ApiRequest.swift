//
//  ApiRequest.swift
//  News Reader
//
//  Created by Arjay on 9/30/20.
//  Copyright © 2020 Arjay. All rights reserved.
//

import Foundation

enum NewsError:Error {
    case noDataAvailable
}

struct ApiRequest {
    
    let url: URL
    let url2: URL
    
    init() {
        
        let currentDate = Date()
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd"
        let dateString = format.string(from: currentDate)
        //print(dateString)
        
        
        let urlHeadline = "https://newsapi.org/v2/top-headlines?country=ph&apiKey=768c01b006c64da3be95222046e94fd1"
        guard let url = URL(string: urlHeadline) else {fatalError()}
        self.url = url
        
        let urlNews = "https://newsapi.org/v2/everything?q=philippines&from=\(dateString)&sortBy=publishedAt&apiKey=768c01b006c64da3be95222046e94fd1"
        guard let url2 = URL(string: urlNews) else {fatalError()}
        self.url2 = url2
        
    }
    
    func getHeadline(completion: @escaping(Result<[Article], NewsError>) -> Void){
        
        let dataTask = URLSession.shared.dataTask(with: url) { (data, _, _) in
            guard let jsonData = data else {
                completion(.failure(.noDataAvailable))
                return
            }
            
            do{                
                let decoder = JSONDecoder()
                let feedResponse = try decoder.decode(Feed.self, from: jsonData)
                let feed = feedResponse.articles
                completion(.success(feed!))
            }catch {
                completion(.failure(.noDataAvailable))
            }
            
        }
        
        dataTask.resume()

    }
    
    func getNews(completion: @escaping(Result<[Article], NewsError>) -> Void){
        
        let dataTask = URLSession.shared.dataTask(with: url2) { (data, _, _) in
            guard let jsonData = data else {
                completion(.failure(.noDataAvailable))
                return
            }
            
            do{
                
                let decoder = JSONDecoder()
                let feedResponse = try decoder.decode(Feed.self, from: jsonData)
                let feed = feedResponse.articles
                completion(.success(feed!))
            }catch {
                completion(.failure(.noDataAvailable))
                
            }
            
        }
        
        dataTask.resume()

    }
    
}

